#include "health.h"
#include <QFont>

Health::Health(QGraphicsItem* parent): QGraphicsTextItem(parent) {
    health = 5;

    setPlainText(QString(QString::number(health)));
    setDefaultTextColor(Qt::red);
    setFont(QFont("arial Black", 18));
}

void Health::decrease() {
    health--;
    setPlainText(QString(QString::number(health)));
}

int Health::getHealth() {
    return health;
}
