#include "brick.h"
#include <QBrush>
#include "game.h"

//extern Game * game;

Brick::Brick(QGraphicsItem *parent): QGraphicsRectItem(parent){
    // draw rect
    setRect(0,0,70,30);

    //style rect (bricks)
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::yellow);
    setBrush(brush);
}
