#ifndef BALL_H
#define BALL_H

#include <QGraphicsEllipseItem>
#include <QTimer>

//class QTimer;

class Ball : public QObject, public QGraphicsEllipseItem {
    Q_OBJECT
public:
    Ball();
    int getCenterX();
    bool flagMoving = false;

    void resetPosition();


public slots:
    void move();
    void startMoving();
    void stopMoving();

private:
    int xMoving;
    int yMoving;
    QTimer* timer;

    void reverseVelocityIfOutOfBounds();
    void handlePlatformCollision();
    void handleBrickCollision();
};

#endif // BALL_H
