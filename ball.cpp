#include "ball.h"
#include "game.h"
#include "brick.h"
#include "platform.h"
#include <QTimer>

extern Game* game;

Ball::Ball()
{
    // draw rect
    setRect(0, 0, 35, 35);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::blue);
    setBrush(brush);

    // move up right initially
    xMoving = 0;
    yMoving = -5;
    timer = nullptr;
}

void Ball::startMoving()
{
    if (!timer) {
        timer = new QTimer(this);
        connect(timer, SIGNAL(timeout()), this, SLOT(move()));
    }
    timer->start(15);
}

void Ball::stopMoving()
{
    if (timer) {
        timer->stop();
    }
}

int Ball::getCenterX()
{
    return (x() + rect().width() / 2);
}

void Ball::move()
{
    reverseVelocityIfOutOfBounds(); // Отталкивание
    handlePlatformCollision();      // Для платформы
    handleBrickCollision();         // Для кирпичей

    moveBy(xMoving, yMoving);
}

void Ball::reverseVelocityIfOutOfBounds()
{
    int screenW = game->width();
    int screenH = game->height();

    if (mapToScene(rect().topLeft()).x() <= 0 || mapToScene(rect().topRight()).x() >= screenW) {
        xMoving = -1 * xMoving;
    }

    if (mapToScene(rect().topLeft()).y() <= 0) {
        yMoving = -1 * yMoving;
    }

    if (mapToScene(rect().topLeft()).y() >= screenH) {
        // Возвращаем шарик в исходную позицию
        setPos(350, 570);

        // Останавливаем таймер
        stopMoving();

        game->health->decrease();
    }
}

void Ball::resetPosition()
{
    setPos(350, 570);
    stopMoving();
}

void Ball::handlePlatformCollision()
{
    QList<QGraphicsItem*> cItems = collidingItems();
    for (int i = 0; i < cItems.size(); ++i) {
        Platform* platform = dynamic_cast<Platform*>(cItems[i]);
        if (platform) {
            yMoving = -1 * yMoving;
            int ballX = getCenterX();
            int platformX = platform->getCenterX();
            int dvx = ballX - platformX;
            xMoving = (xMoving + dvx) / 15;
            return;
        }
    }
}

void Ball::handleBrickCollision()
{
    QList<QGraphicsItem*> cItems = collidingItems();
    for (int i = 0; i < cItems.size(); ++i) {
        Brick* brick = dynamic_cast<Brick*>(cItems[i]);
        // collides with block
        if (brick) {
            int yPad = 15;
            int xPad = 15;
            int ballx = pos().x();
            int bally = pos().y();
            int blockx = brick->pos().x();
            int blocky = brick->pos().y();

            if (bally > blocky + yPad && yMoving < 0) {
                yMoving = -1 * yMoving;
            }
            if (blocky > bally + yPad && yMoving > 0) {
                yMoving = -1 * yMoving;
            }
            if (ballx > blockx + xPad && xMoving < 0) {
                xMoving = -1 * xMoving;
            }
            if (blockx > ballx + xPad && xMoving > 0) {
                xMoving = -1 * xMoving;
            }

            // delete brick
            game->scene->removeItem(brick);

            delete brick;
            game->score->increase();

            // Уменьшаем счетчик кирпичей
            game->brickCount--;

            // Если нет больше кирпичей на сцене, вызываем функцию showWinMenu()
            if (game->brickCount == 0) {
                game->showWinMenu();
                game->gameOver = true;
                timer->stop();
            }


            return;
        }
    }
}
