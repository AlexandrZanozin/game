#ifndef PLATFORM_H
#define PLATFORM_H

#include <QGraphicsRectItem>
#include <QGraphicsSceneMouseEvent>
#include <QKeyEvent>
#include <QTimer>

class Platform: public QGraphicsRectItem{

public:
    Platform();

    int getCenterX();

    void mouseMoveEvent(QGraphicsSceneMouseEvent* event);

};

#endif // PLATFORM_H
