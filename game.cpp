#include "game.h"
#include "platform.h"
#include "ball.h"
#include "brick.h"
#include "score.h"
#include "health.h"
#include "button.h"

#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QKeyEvent>
#include <QTimer>
#include <QApplication>
#include <QObject>

Game::Game(QWidget* parent)
{
    scene = new QGraphicsScene();
    scene->setSceneRect(0, 0, 700, 700);

    setScene(scene);
    setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    setFixedSize(700, 700);
}

Game::~Game()
{
    delete ball;
    delete platform;
    delete score;
    delete health;
    delete timer;
    delete startButton;
    delete restartButton;
    delete quitButton;
}

void Game::showMenu()
{
    startButton = new Button("Start");
    int buttonWidth = 200;
    int buttonHeight = 50;
    int buttonX = width() / 2 - buttonWidth / 2;
    int buttonY = height() / 2 - buttonHeight / 2;
    startButton->setPos(buttonX, buttonY);
    scene->addItem(startButton);

    connect(startButton, SIGNAL(clicked()), this, SLOT(start()));

    quitButton = new Button("Quit");
    quitButton->setPos(buttonX, buttonY + buttonHeight + 10);
    scene->addItem(quitButton);

    connect(quitButton, SIGNAL(clicked()), this, SLOT(quit()));

    QGraphicsTextItem* titleText = new QGraphicsTextItem("ARKANOID");
    QFont titleFont("comic sans", 50);
    titleText->setFont(titleFont);
    int titleX = width() / 2 - titleText->boundingRect().width() / 2;
    int titleY = 150;
    titleText->setPos(titleX, titleY);
    scene->addItem(titleText);

}

void Game::showGameOverMenu()
{
    scene->clear();
    setBackgroundBrush(QBrush(Qt::black));

    restartButton = new Button("Restart");
    int buttonWidth = 200;
    int buttonHeight = 50;
    int buttonX = width() / 2 - buttonWidth / 2;
    int buttonY = height() / 2 - buttonHeight / 2;
    restartButton->setPos(buttonX, buttonY);
    scene->addItem(restartButton);

    connect(restartButton, SIGNAL(clicked()), this, SLOT(start()));

    quitButton = new Button("Quit");
    quitButton->setPos(buttonX, buttonY + buttonHeight + 10);
    scene->addItem(quitButton);

    connect(quitButton, SIGNAL(clicked()), this, SLOT(quit()));

    QGraphicsTextItem* gameOverText = new QGraphicsTextItem("GAME OVER");
    QFont gameOverFont("comic sans", 50);
    gameOverText->setFont(gameOverFont);

    int gameOverX = width() / 2 - gameOverText->boundingRect().width() / 2;
    int gameOverY = 150;
    gameOverText->setDefaultTextColor(Qt::white);
    gameOverText->setPos(gameOverX, gameOverY);
    scene->addItem(gameOverText);

}

void Game::showWinMenu()
{
    scene->clear();
    setBackgroundBrush(QBrush(Qt::black));

    restartButton = new Button("Restart");
    int buttonWidth = 200;
    int buttonHeight = 50;
    int buttonX = width() / 2 - buttonWidth / 2;
    int buttonY = height() / 2 - buttonHeight / 2;
    restartButton->setPos(buttonX, buttonY);
    scene->addItem(restartButton);

    connect(restartButton, SIGNAL(clicked()), this, SLOT(start()));

    quitButton = new Button("Quit");
    quitButton->setPos(buttonX, buttonY + buttonHeight + 10);
    scene->addItem(quitButton);

    connect(quitButton, SIGNAL(clicked()), this, SLOT(quit()));

    QGraphicsTextItem* winText = new QGraphicsTextItem("YOU WIN");
    QFont winFont("comic sans", 50);
    winText->setFont(winFont);

    int winX = width() / 2 - winText->boundingRect().width() / 2;
    int winY = 150;
    winText->setDefaultTextColor(Qt::white);
    winText->setPos(winX, winY);
    scene->addItem(winText);
}

void Game::quit()
{
    QApplication::quit();
}

void Game::start()
{
    setBackgroundBrush(QBrush(Qt::black));

    initializeGame();

    if (health->getHealth() <= 1) {
        showGameOverMenu();
        gameOver = true;
    }

    //Проверяем, если нет больше кирпичей на сцене, то вызываем функцию showWinMenu()
    if (brickCount == 0) {
        showWinMenu();
        gameOver = true;

        timer->stop();
    }


}

void Game::initializeGame()
{
    gameOver = false;

    scene->clear();

    ball = new Ball();
    int ballSize = 35;
    int ballX = width() / 2 - ball->boundingRect().width() / 2;
    int ballY = height() - ball->boundingRect().height() - ballSize;
    ball->setPos(ballX, ballY);
    scene->addItem(ball);

    platform = new Platform();
    platform->setPos(350, 675);
    scene->addItem(platform);
    platform->grabMouse();

    score = new Score();
    scene->addItem(score);
    score->setPos(0, 0);

    health = new Health();
    scene->addItem(health);
    health->setPos(width() - health->boundingRect().width(), 0);

    createBlockRow();



    //Подсчет количества кирпичей
    brickCount = scene->items().size() - 4;
    QList<QGraphicsItem*> items = scene->items();
    for (int i = 0; i < items.size(); i++) {
        Brick* brick = dynamic_cast<Brick*>(items[i]);
        if (brick) {
            brickCount++;
        }
    }


    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(update()));
    timer->start(1000 / 60);
}

void Game::keyPressEvent(QKeyEvent* event)
{
    if (gameOver) {
        if (event->key() == Qt::Key_Space) {
            start();
            event->accept();
        } else {
            QGraphicsView::keyPressEvent(event);
        }
    } else if (health->getHealth() == 0) {
        if (event->key() == Qt::Key_Space) {
            showGameOverMenu();
            gameOver = true;
            event->accept();
        } else {
            QGraphicsView::keyPressEvent(event);
        }
    } else {
        if (event->key() == Qt::Key_Space) {
            ball->startMoving();
            event->accept();
        } else {
            QGraphicsView::keyPressEvent(event);
        }
    }
}

void Game::createBlockCol(int x)
{
    int brickHeight = 30;
    int spacing = 5;
    int offsetY = 50;

    for (int i = 0; i < 8; i++) {
        Brick* brick = new Brick();
        int y = offsetY + i * (brickHeight + spacing);
        brick->setPos(x, y);
        scene->addItem(brick);
    }
}

void Game::createBlockRow()
{
    int brickWidth = 70;
    int spacing = 5;
    int offsetX = (scene->width() - 8 * (brickWidth + spacing) + spacing) / 2;

    for (int i = 0; i < 8; i++) {
        int x = offsetX + i * (brickWidth + spacing);
        createBlockCol(x);
    }
}
