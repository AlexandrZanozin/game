#ifndef BRICK_H
#define BRICK_H

#include <QGraphicsRectItem>
#include <QTimer>

class QTimer;

class Brick : public QGraphicsRectItem {
public:
    // constructors
    Brick(QGraphicsItem* parent = nullptr);

private:
    QTimer* brickTimer;
};

#endif // BRICK_H

