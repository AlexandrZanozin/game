#include "platform.h"
#include <QBrush>
#include "game.h"

extern Game * game;

Platform::Platform(){
    // draw rect
    setRect(0,0,100,15);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::green);
    setBrush(brush);
}

int Platform::getCenterX(){
    return (x()+rect().width()/2);
}

void Platform::mouseMoveEvent(QGraphicsSceneMouseEvent *event){
    int mouseX = mapToScene(event->pos()).x();
    setPos(mouseX - rect().width()/2,y());

    QTransform mirrorTransform(-1, 0, 0, 1, rect().width(), 0);
    setTransformOriginPoint(rect().center());
    if (mouseX < game->width()/2) {
        setTransform(mirrorTransform);
    }
    else {
        setTransform(QTransform());
    }
}


