#ifndef GAME_H
#define GAME_H

#include <QGraphicsView>
#include <QGraphicsScene>
#include <QGraphicsTextItem>
#include <QBrush>
#include <QObject>

#include "ball.h"
#include "platform.h"
#include "brick.h"
#include "score.h"
#include "health.h"
#include "button.h"

class Game : public QGraphicsView {
    Q_OBJECT
public:
    Game(QWidget* parent = nullptr);
    ~Game();

    QGraphicsScene* scene;
    Ball* ball;
    Platform* platform;
    QTimer* timer;
    Score* score;
    Health* health;

    bool gameOver = false;
    int brickCount = 0;

    void createBlockCol(int x);
    void createBlockRow();
    void keyPressEvent(QKeyEvent* event);
    void stopGame();
    void startGame();
    void initializeGame();

public slots:
    // Menu
    void showMenu();
    void showGameOverMenu();
    void showWinMenu();
    void start();
    void quit();

private:
    Button* startButton;
    Button* quitButton;
    Button* restartButton;

};

#endif // GAME_H
