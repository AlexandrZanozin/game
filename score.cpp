#include "score.h"
#include <QFont>

Score::Score(QGraphicsItem* parent): QGraphicsTextItem(parent) {
    score = 0;

    setPlainText(QString(QString::number(score)));
    setDefaultTextColor(Qt::white);
    setFont(QFont("arial Black", 18));
}

void Score::increase() {
    score++;
    setPlainText(QString(QString::number(score)));
}

int Score::getScore() {
    return score;
}

