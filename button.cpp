#include "button.h"

Button::Button(QString name, QGraphicsItem *parent) : QGraphicsRectItem(parent) {
    // draw the rect
    setRect(0, 0, 200, 50);
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::white); // установка белого цвета фона кнопки
    setPen(QPen(Qt::black));
    setBrush(brush);

    // draw the text
    text = new QGraphicsTextItem(name, this);
    text->setDefaultTextColor(Qt::blue); // установка синего цвета текста
    text->setFont(QFont("Arial Black", 16, 700));
    int xPos = rect().width()/2 - text->boundingRect().width()/2;
    int yPos = rect().height()/2 - text->boundingRect().height()/2;
    text->setPos(xPos, yPos);

    // allow responding to hover events
    setAcceptHoverEvents(true);
}

void Button::mousePressEvent(QGraphicsSceneMouseEvent *event) {
    emit clicked();
}

void Button::hoverEnterEvent(QGraphicsSceneHoverEvent *event) {
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::white); // установка белого цвета фона кнопки при наведении курсора мыши
    setBrush(brush);
    text->setDefaultTextColor(Qt::black); // установка черного цвета текста при наведении курсора мыши
}

void Button::hoverLeaveEvent(QGraphicsSceneHoverEvent *event) {
    QBrush brush;
    brush.setStyle(Qt::SolidPattern);
    brush.setColor(Qt::white); // установка белого цвета фона кнопки при уходе курсора мыши
    setBrush(brush);
    text->setDefaultTextColor(Qt::blue); // установка синего цвета текста при уходе курсора мыши
}
